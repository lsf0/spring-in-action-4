package spittr.web;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller //组件扫描器会自动找到HomeController， 并将其声明为Spring应用上下文中的一个bean。
@RequestMapping("/")
public class HomeController {

    //@RequestMapping(method = GET)
    @GetMapping
    public String home(Model model) {
        return "home";
    }

}
