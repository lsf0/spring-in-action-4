package spittr.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import spittr.web.WebConfig;
//Servlet 3下的Java配置
/*
扩展AbstractAnnotationConfigDispatcherServletInitializer的任意类都会自动地
配置DispatcherServlet和Spring应用上下文，Spring的应用上下文会位于应用程序的Servlet上下文之中
*/

public class SpitterWebInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    //方法返回的带有@Configuration注解的类将会用来配置ContextLoaderListener创建的应用上下文中的bean
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[]{RootConfig.class};
    }

    //要求DispatcherServlet加载应用上下文时，使用定义在WebConfig配置类（使用Java配置）中的bean
    //方法返回的带有@Configuration注解的类将会用来定义DispatcherServlet应用上下文中的bean
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[]{WebConfig.class};
    }

    //将一个或多个路径映射到DispatcherServlet上。  它映射的是“/”，它会处理进入应用的所有请求。
    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

}