package soundsystem;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;

@Configuration
@Import(CDPlayerConfig.class)//使用@Import导入配置类
@ImportResource("classpath:cd-config.xml")//使用@ImportResource引用XML配置
public class SoundSystemConfig {

}
