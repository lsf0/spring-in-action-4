package soundsystem;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CDPlayerConfig {
  /*@Bean注解会告诉Spring这个方法将会返回一个对象， 该对象要注册
  为Spring应用上下文中的bean。 方法体中包含了最终产生bean实例的
  逻辑。 默认情况下，bean的ID与带有@Bean注解的方法名是一样的*/
  
  @Bean
  public CompactDisc compactDisc() {
    return new SgtPeppers();
  }
  
  @Bean
  public CDPlayer cdPlayer(CompactDisc compactDisc) {
    return new CDPlayer(compactDisc);
  }

}
