package contacts;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;


@Controller
@RequestMapping("/")
public class ContactController {

    @Resource
    private ContactRepository contactRepo;

    @GetMapping
    public String home(Map<String, Object> model) {
        List<Contact> contacts = contactRepo.findAll();
        model.put("contacts", contacts);
        return "home";
    }

    @PostMapping
    public String submit(Contact contact) {
        contactRepo.save(contact);
        return "redirect:/";
    }


}