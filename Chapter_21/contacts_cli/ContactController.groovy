@Grab("spring-boot-starter-actuator")
@Grab("thymeleaf-spring4")

//通过@Grab显示声明导入相关模块

@Controller
@RequestMapping("/")
class ContactController {

	@Autowired
	ContactRepository contactRepo

	@GetMapping
	String home(Map<String,Object> model) {
		List<Contact> contacts = contactRepo.findAll()
		model.putAll([contacts: contacts])
		"home"
	}

	@PostMapping
	String submit(Contact contact) {
		contactRepo.save(contact)
		"redirect:/"
	}

}

/*
Groovy默认会导入一些包和类， 包括：
java.io.*
java.lang.*
java.math.BigDecimal
java.math.BigInteger
java.net.*
java.util.*
groovy.lang.*
groovy.util.*
*/

/*
CLI将会识别出Spring类型，会获取Spring Boot Web Starter
依赖并将其依赖的其他内容都添加到类路径下
*/
