class Contact {
	long id
	String firstName
	String lastName
	String phoneNumber
	String emailAddress
}

//编写Groovy代码的时候， 可以省略如下的内容：
//分号；
//像public和private这样的修饰符；
//属性的Setter和Getter方法；
//方法返回值的return关键字。