package com.habuma.restfun;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MagicConfig {

  @Bean
  @Conditional(MagicExistsCondition.class) //如果类中matches()方法返回true，那么就会创建带有@Conditional注解的bean
  public MagicBean magicBean() {
    return new MagicBean();
  }
  
}