package com.soundsystem;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class SpEL {

    @Value("#{{'是':true,'否':false}}")
    public Map<String, Boolean> map;

    @Value("#{{2,3,5,7}}")
    public List<Integer> list;

    @Value("#{blankDisc.title}")
    public String s;


}
