package com.soundsystem;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:/com/soundsystem/app.properties")
public class BlankDisc {
    //属性值直接注入
    @Value("${disc.title}")
    private String title;

    private final String artist;

    //构造器注入
    public BlankDisc(@Value("${disc.artist}") String artist) {
        this.artist = artist;
    }

    public String getTitle() {
        return title;
    }

    public String getArtist() {
        return artist;
    }

}
