package com.soundsystem;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = BlankDiscConfig.class)
public class SpELTest {

    @Autowired
    private BlankDisc blankDisc;

    @Resource
    private SpEL spEL;

    @Test
    public void assertBlankDiscProperties() {
        assertEquals("The Beatles", blankDisc.getArtist());
        assertEquals("Sgt. Peppers Lonely Hearts Club Band", blankDisc.getTitle());
        System.out.println(spEL.s);
        System.out.println(spEL.list);
        System.out.println(spEL.map.get("是"));
    }


}