package jmx;

import javax.management.Notification;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedNotification;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.jmx.export.notification.NotificationPublisher;
import org.springframework.jmx.export.notification.NotificationPublisherAware;
import org.springframework.stereotype.Component;

@Component
@ManagedResource(objectName = "bean:name=expbean") // ����JMSע������
@ManagedNotification(notificationTypes = "demo", name = "demo") // ����JMX֪ͨ
public class ExpBean implements NotificationPublisherAware {

	private int i;

	@ManagedOperation
	public void printI() {
		System.out.println(i);
	}

	public int getI() {
		return i;
	}

	@ManagedAttribute
	public void setI(int i) {
		this.i = i;
	}

	{
		new Thread() {
			@Override
			public void run() {
				while (i < Integer.MAX_VALUE) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if (i++ % 3 == 0)
						notificationPublisher.sendNotification(new Notification("demo", this, 0));

				}
			};
		}.start();

	}

	private NotificationPublisher notificationPublisher;

	// ע��notificationPublisher
	public void setNotificationPublisher(NotificationPublisher notificationPublisher) {
		this.notificationPublisher = notificationPublisher;
	}
}
