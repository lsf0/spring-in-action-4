package jmx;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jmx.export.MBeanExporter;
import org.springframework.jmx.export.annotation.AnnotationMBeanExporter;
import org.springframework.jmx.support.ConnectorServerFactoryBean;

@Configuration
public class Config {

	// 配置注解驱动
	@Bean
	public MBeanExporter exporter() {
		return new AnnotationMBeanExporter();
	}

	// 配置远程连接
	@Bean
	public ConnectorServerFactoryBean serverFactory() {
		ConnectorServerFactoryBean csfb = new ConnectorServerFactoryBean();
		/*
		 * 默认情况下，服务器使用JMXMP协议并监听端口9875——因此，它将绑定 “service:jmx:jmxmp://localhost:9875”。
		 * 要使用jconsole访问它，需要将依赖
		 * "org.glassfish.main.external:jmxremote_optional-repackaged:+"
		 * 对应的jar包重命名为“jmxremote_optional.jar”并拷贝到客户端
		 * %JAVA_HOME%/jre/lib/ext目录下，再启动jconsole
		 */
		return csfb;
	}

}
