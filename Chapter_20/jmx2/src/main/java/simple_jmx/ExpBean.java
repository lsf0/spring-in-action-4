package jmx;

import org.springframework.stereotype.Component;

@Component
public class ExpBean {

	private int i;

	public int getI() {
		System.out.println("someone got i's value " + i);
		return i;
	}

	public void setI(int i) {
		System.out.println("i is set to " + i);
		this.i = i;
	}
	
	public void print() {
		System.out.println("hello JMX");
	}

	{
		new Thread() {
			@Override
			public void run() {
				while (i < Integer.MAX_VALUE) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					i++;
				}
			};
		}.start();

	}
}
