package jmx;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jmx.export.MBeanExporter;

@Configuration
public class Config {

	/*
	 * Spring的JMX模块可以让我们将Spring bean导出为模型MBean，
	 * 这样我 们就可以在应用的运行期使用基于JMX的管理工具（例如JConsole或者VisualVM）
	 * 查看应用程序的内部情况并且能够更改配置
	 */

	@Bean
	public MBeanExporter exporter(ExpBean bean) {
		MBeanExporter exporter = new MBeanExporter();
		Map<String, Object> beans = new HashMap<String, Object>();
		beans.put("bean:name=expbean", bean);
		exporter.setBeans(beans);
		return exporter;
	}

}
