package jmx;

import java.io.IOException;

import javax.management.*;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ClientMain {

	@SuppressWarnings("resource")
	public static void main(String[] args)
			throws IOException, InstanceNotFoundException, IntrospectionException, MalformedObjectNameException,
			ReflectionException, AttributeNotFoundException, InvalidAttributeValueException, MBeanException {
		ApplicationContext context = new AnnotationConfigApplicationContext("jmx");
		MBeanServerConnection conn = context.getBean(MBeanServerConnection.class);
		print("远程MBean服务器中有", conn.getMBeanCount(), "已注册的MBean");

		for (ObjectInstance o : conn.queryMBeans(null, null))
			print(o.getClassName(), o.getObjectName());

		String name = "bean:name=expbean";
		ObjectName objectName = new ObjectName(name);
		MBeanInfo info = conn.getMBeanInfo(objectName);

		for (MBeanAttributeInfo attr : info.getAttributes())
			print(attr.getType(), attr.getName(), attr.getDescription());

		for (MBeanOperationInfo oper : info.getOperations())
			print(oper.getReturnType(), oper.getName(), oper.getDescription());

		conn.setAttribute(objectName, new Attribute("I", 2));
		conn.invoke(objectName, "printI", null, new String[0]);
	}

	private static void print(Object... objects) {
		for (Object object : objects)
			System.out.print(object + "\t");
		System.out.println();
	}

}
