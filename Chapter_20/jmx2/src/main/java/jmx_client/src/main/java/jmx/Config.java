package jmx;


import java.net.MalformedURLException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.jmx.support.MBeanServerConnectionFactoryBean;

@Configuration
public class Config {	
	
	@Bean
	public MBeanServerConnectionFactoryBean connectionFactor() throws MalformedURLException {
		MBeanServerConnectionFactoryBean mbscfb=new MBeanServerConnectionFactoryBean();
		mbscfb.setServiceUrl("service:jmx:jmxmp://localhost:9875");
		return mbscfb;		
	}
	
	
}
