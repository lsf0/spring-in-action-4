package rmi.service;

import rmi.pojo.Dog;
import rmi.pojo.Info;

public interface InfoService {
    Info ask(Dog dog);
}
