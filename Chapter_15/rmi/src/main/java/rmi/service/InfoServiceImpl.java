package rmi.service;


import rmi.pojo.Dog;
import rmi.pojo.Info;

import java.util.Date;


public class InfoServiceImpl implements InfoService {

    public Info ask(Dog dog) {
        Info info=new Info();
        info.price=(dog.gender ? 2 : 3) * 100 / (dog.age + Math.random());
        info.birthday=new Date();
        return info;
    }
}
