package rmi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiServiceExporter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import rmi.service.InfoService;
import rmi.service.InfoServiceImpl;

@Configuration
public class WebInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {


    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[]{this.getClass()};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[0];
    }

    @Override
    protected String[] getServletMappings() {
        return new String[0];
    }

    @Bean
    public InfoService infoService(){
        return new InfoServiceImpl();
    }

    @Bean
    public RmiServiceExporter rmiExporter(InfoService infoService){
        RmiServiceExporter rse=new RmiServiceExporter();
        rse.setService(infoService);
        rse.setServiceName(InfoService.class.getName());
        rse.setServiceInterface(InfoService.class);
        return rse;
    }


}