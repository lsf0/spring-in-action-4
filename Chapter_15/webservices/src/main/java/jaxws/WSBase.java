package jaxws;

import jaxws.pojo.Dog;
import jaxws.service.InfoService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan("jaxws.service")
@Configuration
public abstract class WSBase {
    protected void test(){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(this.getClass());
        InfoService infoService=context.getBean(InfoService.class);
        System.out.println(infoService.ask(new Dog(1,true)));
    }
}
