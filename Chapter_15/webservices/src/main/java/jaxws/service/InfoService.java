package jaxws.service;


import jaxws.pojo.Dog;
import jaxws.pojo.Info;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)//注解用于客户端
public interface InfoService {
    Info ask(Dog dog);
}
