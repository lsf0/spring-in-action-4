package jaxws.service;

import org.springframework.stereotype.Component;

@Component
public class RndBean {
    public double rnd(){
        return Math.random();
    }
}
