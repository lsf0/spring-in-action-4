package jaxws.service;


import jaxws.pojo.Dog;
import jaxws.pojo.Info;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Date;


@Component
@Profile("server")
@WebService//这里可以设置servicename控制服务的URL
public class InfoServiceImpl implements InfoService {

    @Resource
    private RndBean rndBean;

    @WebMethod
    public Info ask(Dog dog) {
        Info info=new Info();
        info.price=(dog.gender ? 2 : 3) * 100 / (dog.age + Math.random())+rndBean.rnd();
        info.birthday=new Date();
        return info;
    }
}
