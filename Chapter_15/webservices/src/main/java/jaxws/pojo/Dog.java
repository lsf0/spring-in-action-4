package jaxws.pojo;


public class Dog {

    public Dog(int age, boolean gender) {
        this.age = age;
        this.gender = gender;
    }

    public Dog() {
    }

    public int age;

    public String color;

    public boolean gender;

}
