package jaxws.pojo;

import java.util.Date;

public class Info{

    public double price;

    public Date birthday;

    @Override
    public String toString() {
        return "Info{" +
                "price=" + price +
                ", birthday=" + birthday +
                '}';
    }
}
