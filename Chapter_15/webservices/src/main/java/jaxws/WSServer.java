package jaxws;


import org.springframework.context.annotation.Bean;
import org.springframework.remoting.jaxws.SimpleJaxWsServiceExporter;


public class WSServer extends WSBase{
    public static void main(String[] args) {
        System.setProperty("spring.profiles.active","server");
        new WSServer().test();
    }

    @Bean
    public SimpleJaxWsServiceExporter jaxWsServiceExporter(){
        SimpleJaxWsServiceExporter serviceExporter=new SimpleJaxWsServiceExporter();
        serviceExporter.setBaseAddress("http://localhost:8888/");//默认为http://localhost:8080/
        return serviceExporter;
    }
}
