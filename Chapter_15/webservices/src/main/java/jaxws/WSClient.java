package jaxws;

import jaxws.service.InfoService;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.jaxws.JaxWsPortProxyFactoryBean;

import java.net.MalformedURLException;
import java.net.URL;

public class WSClient extends WSBase{

    public static void main(String[] args) {
       new WSClient().test();
    }

    @Bean
    public JaxWsPortProxyFactoryBean jaxWsPortProxy() throws MalformedURLException {
        JaxWsPortProxyFactoryBean proxy=new JaxWsPortProxyFactoryBean();
        proxy.setWsdlDocumentUrl(new URL("http://localhost:8888/?wsdl"));
        proxy.setServiceName("InfoServiceImplService");
        proxy.setPortName("InfoServiceImplPort");
        proxy.setServiceInterface(InfoService.class);
        proxy.setNamespaceUri("http://service.jaxws/");
        //我们可以在WSDL中找以上属性对应的值
        return proxy;
    }
}
