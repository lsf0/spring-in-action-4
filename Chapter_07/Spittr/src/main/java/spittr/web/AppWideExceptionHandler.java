package spittr.web;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/*
在带有@ControllerAdvice注解的类中，@ExceptionHandler、@InitBinder、@ModelAttribute
注解标注的方法会运用到整个应用程序所有控制器中带有@RequestMapping注解的方法上。
*/
@ControllerAdvice
public class AppWideExceptionHandler {

    @ExceptionHandler(DuplicateSpittleException.class)
    public String handleNotFound() {
        return "error/duplicate";
    }

}
