package spittr.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/fileupload")
public class FileUploadController {

    @GetMapping
    public String uploadForm() {
        return "uploadForm";
    }

    @PostMapping
    public String processUpload(@RequestPart("file") MultipartFile file) {

        System.out.println("---->  " + file.getName() + "  ::  "  + file.getSize());

        return "redirect:/";
    }

}
