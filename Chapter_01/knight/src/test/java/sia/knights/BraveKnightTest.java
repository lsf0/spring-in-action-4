package sia.knights;

import static org.mockito.Mockito.*;

import org.junit.Test;


public class BraveKnightTest {

    @Test
    public void knightShouldEmbarkOnQuest() {
        //创建一个Quest接口的mock实现
        Quest mockQuest = mock(Quest.class);

        BraveKnight knight = new BraveKnight(mockQuest);
        knight.embarkOnQuest();

        //验证Quest的mock实现的embark()方法仅仅被调用了一次
        verify(mockQuest, times(1)).embark();
    }

}
