package amqp;

import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitOperations;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
@Configuration
public class Listener {
	
	//AMQP在消息的生产者以及传递信息的队列之间引入了一种间接的机制：Exchange。
	//AMQP定义了四种不同类型的Exchange，每一种都有不同的路由算法，这些算法决定了是否要将信息放到队列中。
	//四种标准的AMQP Exchange及消息将会路由到该队列的条件：
	//Direct：消息的routing key与binding的routing key直接匹配
	//Topic：消息的routing key与binding的routing key符合通配符匹配
	//Headers：消息参数表中的头信息和值都与bingding参数表中相匹配
	//Fanout：无条件

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		ApplicationContext context = new AnnotationConfigApplicationContext(Listener.class);
		RabbitOperations template = context.getBean(RabbitOperations.class);		
		//传入了三个参数：Exchange的名称（可省略）、routing key（可省略）以及要发送的对象。
		//使用默认的消息转换器SimpleMessageConverter将对象转换为Message
		template.convertAndSend("myExchange", "foo.bar", "Hello, world!");
		//System.out.println(template.receiveAndConvert("myQueue"));
		System.out.println("message has been sent.");

	}

	@Bean
	public ConnectionFactory cf() {
		// 默认情况下，连接工厂会假设RabbitMQ服务器监听localhost的5672端口，并且用户名和密码均为guest。
		CachingConnectionFactory cf = new CachingConnectionFactory();
		cf.setHost("localhost");
		return cf;
	}

	@Bean
	public Queue queue() {
		return new Queue("myQueue");
	}

	@Bean
	public Exchange exchange() {
		return new TopicExchange("myExchange");
	}

	@Bean
	public RabbitAdmin admin(ConnectionFactory cf, Queue queue, TopicExchange exchange) {
		RabbitAdmin admin = new RabbitAdmin(cf);
		//默认会有一个没有名称的direct Exchange，所有的队列都会绑定到这个Exchange上，并且routing key与队列的名称相同。
		admin.declareQueue(queue);
		admin.declareExchange(exchange);
		admin.declareBinding(BindingBuilder.bind(queue).to(exchange).with("foo.*"));
		return admin;
	}

	@Bean
	public MessageListenerAdapter adapter(Listener listener) {
		return new MessageListenerAdapter(listener);
	}

	@Bean
	public SimpleMessageListenerContainer container(ConnectionFactory cf, MessageListenerAdapter adapter) {
		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(cf);
		container.setMessageListener(adapter);
		container.setQueueNames("myQueue");
		container.start();
		return container;
	}

	public void handleMessage(String foo) {
		System.out.println(foo);
	}

	@Bean
	public RabbitOperations template(ConnectionFactory cf) {
		return new RabbitTemplate(cf);
	}

}
