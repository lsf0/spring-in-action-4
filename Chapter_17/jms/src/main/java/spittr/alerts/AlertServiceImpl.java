package spittr.alerts;

import org.springframework.jms.core.JmsOperations;

import spittr.domain.Spittle;


public class AlertServiceImpl implements AlertService {

    private JmsOperations jmsOperations;

    public AlertServiceImpl(JmsOperations jmsOperations) {
        this.jmsOperations = jmsOperations;
    }

  /*public void sendSpittleAlert(final Spittle spittle) {
    jmsOperations.send(
    "spittle.alert.queue",//JMS目的地名称，若已指定默认，可省略
            s -> s.createObjectMessage(spittle)//返回一个对象消息
    );
  }*/


  public void sendSpittleAlert(Spittle spittle) {
    jmsOperations.convertAndSend(spittle);
  }


  public Spittle retrieveSpittleAlert() {
    return (Spittle) jmsOperations.receiveAndConvert();
  }

}
