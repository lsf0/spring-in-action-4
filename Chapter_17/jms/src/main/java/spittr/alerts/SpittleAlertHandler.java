package spittr.alerts;

import org.springframework.jms.annotation.JmsListener;
import spittr.domain.Spittle;

public class SpittleAlertHandler {

  @JmsListener(destination = "spittle.alert.queue")
  public void handleSpittleAlert(Spittle spittle) {
    System.out.println(spittle.getMessage());
    System.out.println("text");
  }

}
