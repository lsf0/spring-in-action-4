package spittr;

import java.util.Date;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import spittr.alerts.AlertService;
import spittr.domain.Spittle;

public class SpittleJmsMain {

    public static void main(String[] args) {

        ApplicationContext context = new AnnotationConfigApplicationContext("spittr");
        AlertService alertService = context.getBean(AlertService.class);

        Spittle spittle = new Spittle(1L, null, "Hello", new Date());
        //alertService.sendSpittleAlert(spittle);
        //System.out.println(alertService.retrieveSpittleAlert());

    }

}
