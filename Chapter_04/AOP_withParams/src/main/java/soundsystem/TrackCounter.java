package soundsystem;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@Aspect
public class TrackCounter {
    private Map<Integer, Integer> trackCounters = new HashMap<>();

    @Pointcut("execution(* soundsystem.CompactDisc.playTrack(int)) && args(trackNumber)")
    public void trackPlayed(int trackNumber) {
    }

    @Before("trackPlayed(trackNumber)")
    public void countTrack(int trackNumber) {
        trackCounters.put(trackNumber, getPlayCount(trackNumber) + 1);
    }

    public int getPlayCount(int trackNumber) {
        return trackCounters.getOrDefault(trackNumber, 0);
    }

}
