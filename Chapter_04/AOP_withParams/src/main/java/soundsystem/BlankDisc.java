package soundsystem;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class BlankDisc implements CompactDisc {

    private final String title = "Sgt. Pepper's Lonely Hearts Club Band";
    private final String artist = "The Beatles";
    private final List<String> tracks = Arrays.asList("Sgt. Pepper's Lonely Hearts Club Band", "With a Little Help from My Friends",
            "Lucy in the Sky with Diamonds", "Getting Better", "Fixing a Hole", "She's Leaving Home",
            "Being for the Benefit of Mr. Kite!", "Within You Without You", "When I'm Sixty-Four", "Lovely Rita",
            "Good Morning Good Morning", "Sgt. Pepper's Lonely Hearts Club Band (Reprise)", "A Day in the Life");

    @Override
    public void play() {
        System.out.println("Playing " + title + " by " + artist);
        for (String track : tracks)
            System.out.println("-Track: " + track);
    }

    @Override
    public void playTrack(int trackNumber) {
        System.out.println(tracks.get(trackNumber));
    }

}
