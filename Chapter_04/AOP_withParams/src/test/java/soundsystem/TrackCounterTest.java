package soundsystem;

import static org.junit.Assert.assertEquals;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TrackCounterConfig.class)
public class TrackCounterTest {

	@Resource
	private CompactDisc cd;

	@Resource
	private TrackCounter counter;

	@Test
	public void testTrackCounter() {
		int[] trackNumbers = { 1, 2, 3, 3, 3, 3, 7, 7 };

		for (int trackNumber : trackNumbers)
			cd.playTrack(trackNumber);

		int[] trackCounts = { 1, 1, 4, 0, 0, 0, 2 };

		for (int i = 0; i < trackCounts.length; i++)
			assertEquals(trackCounts[i], counter.getPlayCount(i + 1));

	}

}
