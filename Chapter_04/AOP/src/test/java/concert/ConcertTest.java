package concert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import javax.annotation.Resource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ConcertConfig.class, loader = AnnotationConfigContextLoader.class)
public class ConcertTest {

    @Resource
    Performance concert;

    @Test
    public void test() {
        concert.perform();
    }

    @ActiveProfiles("simple")
    public static class AudienceTest extends ConcertTest{
    }

    @ActiveProfiles("around")
    public static class AudienceAroundTest extends ConcertTest{
    }

    @ActiveProfiles("introduce")
    public static class EncoreableIntroducerTest extends ConcertTest{

        @Override
        public void test() {
            super.test();
            ((Encoreable)concert).performEncore();
        }
    }








}