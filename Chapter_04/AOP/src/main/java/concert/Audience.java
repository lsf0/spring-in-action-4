package concert;

import org.aspectj.lang.annotation.*;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Aspect //@Aspect注解表明Audience是一个切面
@Component
@Profile("simple")
public class Audience {

    //@Pointcut注解能够在一个@AspectJ切面内定义可重用的切点
    @Pointcut("execution(** concert.Performance.perform(..))")
    public void performance() {
    }

    @Before("performance()")
    public void silenceCellPhones() {
        System.out.println("Silencing cell phones");
    }

    @Before("performance()")//通知方法会在目标方法调用之前执行
    public void takeSeats() {
        System.out.println("Taking seats");
    }

    @AfterReturning("performance()") //通知方法会在目标方法返回后调用
    public void applause() {
        System.out.println("CLAP CLAP CLAP!!!");
    }

    @AfterThrowing("performance()") //通知方法会在目标方法抛出异常后调用
    public void demandRefund() {
        System.out.println("Demanding a refund");
    }

    //@After() //通知方法会在目标方法返回或抛出异常后调用


}
