package spittr.db.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import spittr.db.SpittleRepository;
import spittr.domain.Spittle;

/*
由于没有使用模板类来处理异常，所以我们需要为Repository添加@Repository注解，
这样PersistenceExceptionTranslationPostProcessor就会
知道要将这个bean产生的异常转换成Spring的统一数据访问异常。
*/

@Repository
public class JpaSpittleRepository implements SpittleRepository {

    /*
    @PersistenceContext没有将真正的EntityManager设置给Repository，而是给了它一个EntityManager的代理。
    真正的EntityManager是与当前事务相关联的那一个，如果不存在这样的EntityManager的话，就会创建一个新的。
    这样的话，我们就能始终以线程安全的方式使用实体管理器。

    */

    @PersistenceContext
    private EntityManager entityManager;

    public long count() {
        return findAll().size();
    }

    public List<Spittle> findRecent() {
        return findRecent(10);
    }

    public List<Spittle> findRecent(int count) {
        return entityManager.createQuery("select s from Spittle s order by s.postedTime desc")
                .setMaxResults(count)
                .getResultList();
    }

    public Spittle findOne(long id) {
        return entityManager.find(Spittle.class, id);
    }

    public Spittle save(Spittle spittle) {
        entityManager.persist(spittle);
        return spittle;
    }

    public List<Spittle> findBySpitterId(long spitterId) {
        return entityManager.createQuery("select s from Spittle s, Spitter sp where s.spitter = sp and sp.id=?1 order by s.postedTime desc")
                .setParameter(1, spitterId)
                .getResultList();
    }

    public void delete(long id) {
        entityManager.remove(findOne(id));
    }

    public List<Spittle> findAll() {
        return entityManager.createQuery("select s from Spittle s").getResultList();
    }

}
