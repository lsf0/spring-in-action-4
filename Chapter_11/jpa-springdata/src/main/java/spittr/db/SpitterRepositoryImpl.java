package spittr.db;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

//Spring Data JPA会把此类的eliteSweep方法作为SpitterRepository的eliteSweep方法的实现
public class SpitterRepositoryImpl{

    @PersistenceContext
    private EntityManager em;

    public int eliteSweep() {
        String update =
                "UPDATE Spitter spitter " +
                        "SET spitter.status = 'Elite' " +
                        "WHERE spitter.status = 'Newbie' " +
                        "AND spitter.id IN (" +
                        "SELECT s FROM Spitter s WHERE (" +
                        "SELECT COUNT(spittles) FROM s.spittles spittles) > 2)";
        return em.createQuery(update).executeUpdate();
    }

}
