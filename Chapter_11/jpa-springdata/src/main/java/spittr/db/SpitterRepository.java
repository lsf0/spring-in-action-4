package spittr.db;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import spittr.domain.Spitter;

/*
JpaRepository进行了参数化，所以它就能知道这是一个用来
持久化Spitter对象的Repository，并且Spitter的ID类型为Long
*/
public interface SpitterRepository extends JpaRepository<Spitter, Long> {

    int eliteSweep();

    Spitter findByUsername(String username);

    List<Spitter> findByUsernameOrFullNameLike(String username, String fullName);

}
