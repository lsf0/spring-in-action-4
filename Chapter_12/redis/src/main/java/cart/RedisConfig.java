package cart;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
//import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

@Configuration
public class RedisConfig {

    @Bean
    public RedisConnectionFactory redisCF() {
        //JedisConnectionFactory cf=new JedisConnectionFactory();
        LettuceConnectionFactory cf=new LettuceConnectionFactory();
        //通过默认构造器创建的连接工厂会向localhost上的6379端口创建连接，并且没有密码。
        //cf.setHostName("redis-server");
        //cf.setPort(7379);
        //cf.setPassword("");
        return cf;
    }

    @Bean
    public RedisTemplate<String, Product> redisTemplate(RedisConnectionFactory cf) {
        RedisTemplate<String, Product> redis = new RedisTemplate<>();
        redis.setConnectionFactory(cf);
        return redis;
    }


    //StringRedisTemplate扩展了RedisTemplate，key和value都是String类型
    @Bean
    public StringRedisTemplate stringRedisTemplate(RedisConnectionFactory cf){
        return new StringRedisTemplate(cf);
    }

}
