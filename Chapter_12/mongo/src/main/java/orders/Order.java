package orders;

import java.util.Collection;
import java.util.LinkedHashSet;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document//标示映射到MongoDB文档上的领域对象
public class Order {

    @Id//标示某个域为ID域
    private String id;

    @Field("client")//映射为名为client的域
    private String customer;

    /*
    除非将属性设置为瞬时态（transient）的，
    否则Java对象中所有的域都会持久化为文档中的域。
    如果不使用@Field注解进行设置的话，那么
    文档域中的名字将会与对应的Java属性相同。
    */

    private String type;

    private Collection<Item> items = new LinkedHashSet<>();

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Collection<Item> getItems() {
        return items;
    }

    public void setItems(Collection<Item> items) {
        this.items = items;
    }

    public String getId() {
        return id;
    }

}
