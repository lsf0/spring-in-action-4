package orders.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;

@Configuration
@EnableMongoRepositories(basePackages = "orders.db")//启用Spring Data的自动化JPA Repository生成功能
public class MongoConfig extends AbstractMongoConfiguration {
    //MongoTemplate bean被隐式地创建

    @Override
    protected String getDatabaseName() {//指定数据库名称
        return "OrdersDB";
    }

    @Override
    public Mongo mongo() throws Exception {//创建Mongo客户端
        return new MongoClient();
    }

}

