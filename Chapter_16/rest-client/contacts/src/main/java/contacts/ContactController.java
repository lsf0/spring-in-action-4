package contacts;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


@RestController
@RequestMapping("/")
public class ContactController {

    private final RestOperations restOperations = new RestTemplate();
    private static ObjectWriter objectWriter = new ObjectMapper().writerWithDefaultPrettyPrinter();

    private static String toJson(Object o) {
        try {
            return objectWriter.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }


    private String prefix;

    @Resource
    private HttpServletRequest request;

    private String getPrefix() {
        if (prefix != null)
            return prefix;
        return "http://" + request.getHeader("host") + "/";
    }

    @RequestMapping("a")
    public Contact home(long id, String s, @RequestParam(defaultValue = "hg") String d) {
        return new Contact(id, s, d, "123", "q@1.cn");
    }

    @PostMapping("b")
    public String s(@RequestBody Contact contact) {
        return contact.toString();
    }


    //RestTemplate定义了36个与REST资源交互的方法，
    // 这里面只有11个独立的方法，其中有十个有三种重载形式，而第十一个则重载了六次
    //*tForEntity()方法会返回请求的对象以及响应相关的额外信息

    @GetMapping
    public void test() throws URISyntaxException {
        //表单提交自动转义

        System.out.println(toJson(restOperations.getForEntity(getPrefix() + "a?id={id}&s={s}", Contact.class, 3, "g")));

        Map<String, Object> map = new HashMap<>();
        map.put("id", 2);
        map.put("s", "你");
        System.out.println(restOperations.getForObject(getPrefix() + "a?id={id}&s={s}", Map.class, map));

        //Post表单提交
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("id", 1);
        body.add("s", "特殊字符&=?");
        System.out.println(toJson(restOperations.postForObject(getPrefix() + "a?d={d}", body, Contact.class, "h")));

        //Post提交JSON
        map.put("phoneNumber", "123456");
        System.out.println(restOperations.postForEntity(getPrefix() + "b", map, String.class));

        System.out.println(restOperations.optionsForAllow(getPrefix() + "b"));

        //exchange可以设置头信息
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("User-Agent", "Mozilla/5.0 (Linux; U; Android 4.1.1; zh-cn; Lenovo P770 Build/JRO03C) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30");
        RequestEntity<String> entity = new RequestEntity<>(headers, HttpMethod.GET, new URI("https://www.baidu.com"));
        System.out.println(restOperations.exchange(entity, String.class));
    }


}